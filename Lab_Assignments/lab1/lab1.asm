	;; Ericta,John
	;; 860803073
	;; Lab Section:01
	;; jeric001@ucr.edu, jericta@cs.ucr.edu
	;; Lab1


	.ORIG x3000
	
	LD R0, ZERO
	LD R1, SIX
	LD R2, NUM

	BRz END

	LOOP 
	ADD R0, R0, R1
	ADD R2, R2, #-1

	BRp LOOP

	END HALT

	SIX .FILL #6

	NUM .FILL #5

	ZERO .FILL #0

	.END