	;; Ericta, John
	;; 3073
	;; jericta
	;; section 001
	;; jericta@cs.ucr.edu
	;; Lab 4: ex2

	.ORIG x3000
	LEA R4, LABEL
	LD R1, COUNT
	
	BRz END
	LOOP
	TRAP x20
	STR R0, R4, #0
	ADD R4, R4, #1	 
	ADD R1, R1, #-1
	BRp LOOP
	END

	LD R1, COUNT
	LEA R4, LABEL
	
	BRz E
	L
	LDR R0, R4, #0
	ADD R0, R0, #1
	TRAP x21
	ADD R4, R4, #1
	ADD R1, R1, #-1
	BRp L
	E
	
	
	LABEL .BLKW #10 
	COUNT .FILL #10 