	;; Ericta, John
	;; 3073
	;; jericta
	;; section 001
	;; jericta@cs.ucr.edu
	;; Lab 4: ex3

	.ORIG x3000
	LD R1, LABEL
	LEA R4, ADDRESS
	LD R2, COUNT
	
	LOOP
	TRAP x20
	STR R0, R4, #0
	ADD R4, R4, #1	 
	ADD R2, R2, #-1
	BRp LOOP

	LEA R0, ADDRESS
	TRAP x22

	HALT
		
	LABEL .STRINGZ "jerictajj"
	COUNT .FILL #10
	ADDRESS .BLKW #11 