	;; John Ericta
	;; 3073
	;; jericta@cs.ucr.edu
	;; Lab9 Ex 1 
	.ORIG x3000

	REV
	LD R0, NEWLINE
	TRAP x21
	LEA R0, PROMPT2		
	TRAP x22
	LD R0, NEWLINE
	TRAP x21

	TRAP x20		 
	LD R5, i
	ADD R3, R3, #-4		; if its zero, its full, 4-4 =0
	BRz ERROR
	ADD R0, R0, R5		; if its i, push
	BRz PUSH
	LD R5, add_back
	ADD R0, R0, R5
	LD R5, o
	ADD R3, R3, #4
	ADD R3, R3, #0		; if its 0, empty
	BRz ERROR
	ADD R0, R0, R5		; if its o, pop
	BRz POP

	PUSH
	
	LEA R0, PROMPT		; Enter character
	TRAP x22
	LD R0, NEWLINE
	TRAP x21
	TRAP x20
	
	LD R1, PUSH_FUNCTION	; jumps to push function
	JSRR R1

	BRnzp END

	POP

	LD R0, NEWLINE
	TRAP x21
	LD R1, POP_FUNCTION	; jumps to pop function
	JSRR R1
	TRAP x21		; outputs when popped
	
	END

	BRnzp REV

	ERROR			; should output error message
	
	HALT

	add_back .FILL #105
	i .FILL #-105
	o .FILL #-111
	POP_FUNCTION .FILL x3570
	PUSH_FUNCTION .FILL x3500
	STACK .FILL x4000
	NEWLINE .FILL #10
	PROMPT .STRINGZ "ENTER A CHARACTER:"
	PROMPT2 .STRINGZ "TYPE [i] FOR PUSH OR [o] FOR POP"
	
	;;-----------------------------------------------

	.ORIG x3500

	ST R7, SAVE7

	LD R3, COUNTER		; counter for character 
	LD R2, ARRAY		; adds counter to array
	ADD R2, R2, R3
	STR R0, R2, #0		; stores the character in the array
	ADD R3, R3, #1		; increments counter
	
	ST R3, COUNTER
	LD R7, SAVE7

	RET 

	SAVE7 .FILL #0
	ARRAY .FILL x3550
	COUNTER .FILL #0

	;; -----------------------------------------------

	
	.ORIG x3570

	ST R7, SAVES7

	LD R3, COUNTER		; subtract counter
	LD R2, ARRAY		; pops character
	ADD R3, R3, #-1
	ADD R2, R2, R3
	LDR R0, R2, #0


	ST R3, COUNTER
	LD R7, SAVES7
	RET

	SAVES7 .FILL #0
	

	
	