	;; John Ericta
	;; jericta
	;; 860803073
	;; Lab 6 Ex 1
	;; Marc Sariano

	.ORIG x3000

	LD R1, PROMPT
	LEA R0, PROMPT
	TRAP x22
	LD R0, NEW_LINE
	TRAP x21

	;; --------------------------------

	;; INPUT 5 DIGIT NUMBER BLOCK

	LD R1, COUNTER
	AND R2, R2, #0

	LOOP

	LD R3, ASC 
	
	TRAP x20
		
	ADD R0, R0, R3

	LD R5, ADDR

	JSRR R5
	
	ADD R2, R2, R0

	ADD R1, R1, #-1
	
	BRp LOOP

	;; ADD 1 AND OUTPUT TO BINARY BLOCK
	LD R6, ASCII
	ADD R2, R2, #1

	LD R1, TEN_THOUSAND_NEG
	LD R4, TEN_THOUSAND_POS
	LD R3, TALLY

	;; TEN THOUSANDTHS PLACE

	DIV1
	
	ADD R3, R3, #1
	ADD R2,R2,R1

	BRzp DIV1

	ADD R2,R2,R4
	ADD R3, R3, #-1
	ADD R0, R3, R6
	TRAP x21

	;; THOUSANDTHS PLACE

	LD R1, THOUSAND_NEG
	LD R4, THOUSAND_POS
	LD R3, TALLY

	DIV2

	ADD R3,R3, #1
	ADD R2, R2, R1
	
	BRzp DIV2

	ADD R2,R2,R4
	ADD R3,R3,#-1
	ADD R0, R3, R6
	TRAP x21

	;; HUNDREDTHS PLACE

	LD R1, HUNDREDTHS_NEG
	LD R4, HUNDREDTHS_POS
	LD R3, TALLY

	DIV3

	ADD R3,R3, #1
	ADD R2, R2, R1
	
	BRzp DIV3

	ADD R2,R2,R4
	ADD R3,R3,#-1
	ADD R0, R3, R6
	TRAP x21

	;; TENS

	LD R1, TENS_NEG
	LD R4, TENS_POS
	LD R3, TALLY

	DIV4

	ADD R3,R3, #1
	ADD R2, R2, R1
	
	BRzp DIV4

	ADD R2,R2,R4
	ADD R3,R3,#-1
	ADD R0, R3, R6
	TRAP x21

	ADD R0, R2, R6
	TRAP x21

	TALLY .FILL #0
	TENS_NEG .FILL #-10
	TENS_POS .FILL #10
	HUNDREDTHS_NEG .FILL #-100
	HUNDREDTHS_POS .FILL #100
	THOUSAND_NEG .FILL #-1000
	THOUSAND_POS .FILL #1000
	TEN_THOUSAND_NEG .FILL #-10000
	TEN_THOUSAND_POS .FILL #10000
	ASCII .FILL#48
	ASC .FILL #-48
	COUNTER .FILL #5
	PROMPT .STRINGZ "ENTER A 5 DIGIT NUMBER"
	NEW_LINE .FILL #10
	ADDR .FILL x4000
	
	;; -------------------------------

	;; MULTIPLICATION BLOCK BY POWERS OF TEN

	.ORIG x4000

	ST R1, SAVE1
	ST R7, SAVE7
	
	LD R3, TEN
	AND R5, R5, #0

	MULT

	ADD R5,R5,R2
	ADD R3, R3, #-1

	BRp MULT

	ADD R2,R5,#0

	LD R1, SAVE1
	LD R7, SAVE7

	RET

	TEN .FILL #10
	SAVE1 .FILL #0
	SAVE2 .FILL #0
	SAVE7 .FILL #0

	;; --------------------------------