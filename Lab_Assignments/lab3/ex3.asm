	;; Ericta, John
	;; 3073
	;; jericta
	;; Section 001
	;; jericta@cs.ucr.edu
	;; Lab 3, Excercise 3
	;; Stores the user inputs into an array 
	;; of ten spaces, ten times	
	
	.ORIG x3000

	LD R1, COUNT
	LD R3, ARRAY

	BRz END

	LOOP
	TRAP x20
	TRAP x21
	STR R0, R3, #0
	ADD R3, R3, #1
	ADD R1, R1,#-1
	BRp LOOP
	
	END
	
	HALT
	
	COUNT .FILL #10
	ARRAY .FILL x4000
