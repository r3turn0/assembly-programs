	;; Ericta, John
	;; 3073
	;; jericta
	;; Section 001
	;; jericta@cs.ucr.edu
	;; Lab 3, Excercise 4
	;; Stores the user inputs into an array 
	;; of ten spaces, ten times and out put those values
	;; into the terminal with new lines

	.ORIG x3000

	LD R1, COUNT
	LD R3, ARRAY

	BRz END

	LOOP
	TRAP x20
	STR R0, R3, #0
	ADD R3, R3, #1
	ADD R1, R1,#-1
	BRp LOOP
	
	END

	LD R3, ARRAY
	LD R1, COUNT

	LOOP1
	LD R0, COUNT
	TRAP x21
	LDR R0, R3, #0
	TRAP x21 
	ADD R3, R3, #1
	ADD R1, R1,#-1
	BRp LOOP1
	
	HALT
	
	COUNT .FILL #10
	ARRAY .FILL x4000