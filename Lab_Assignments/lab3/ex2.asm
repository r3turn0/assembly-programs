	;; Ericta, John
	;; 3073
	;; jericta
	;; Section 001
	;; jericta@cs.ucr.edu
	;; Lab 3, Excercise 2
	;; Stores the power of two into an array 
	;; of ten spaces	
	
	.ORIG x3000
	
	LD R2, NUMBER
	LD R1, COUNT
	LEA R3, ARRAY
	
	BRz END

	LOOP
	STR R2, R3, #0
	ADD R2, R2, R2
	ADD R3, R3, #1
	ADD R1, R1,#-1
	BRp LOOP
	
	END
	
	HALT
	
	COUNT .FILL #10
	NUMBER .FILL #2
	ARRAY .BLKW #10

	
	
	