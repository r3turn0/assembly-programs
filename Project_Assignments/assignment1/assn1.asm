	;; Name: Ericta,John
	;; Login ID: jericta
	;; Email Address: jericta@cs.ucr.edu, jeric001@ucr.edu
	;; Lab Section:	01
	;; TA: Marc Soriano

	.ORIG x3000

	;; INITIALIZE REGISTER VALUES
	
	LD R3, ZERO
	LD R2, NUMBR
	LD R1, SIX

	;; BRANCH OUT WHEN EQUAL ZERO

	BRz END

	;; AGAIN(LOOP) WHILE POSITIVE
	;; R3 = R3+R2
	;; R1 = R1-1
	
	AGAIN
	ADD R3, R3, R2
	ADD R1, R1, #-1
	BRp AGAIN

	;; ENDS LOOP

	END HALT

	;; FILL REGISTERS WITH CONSTANT VALUES
	;; HEXIDECIMAL 6,12,0

	SIX .FILL #6
	NUMBR .FILL #12
	ZERO .FILL #0

	.END