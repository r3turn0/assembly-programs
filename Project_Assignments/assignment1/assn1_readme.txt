Name: Ericta,John
Login ID: jericta
Email Address: jericta@cs.ucr.edu, jeric001@ucr.edu
Lab Section:01
TA: Marc Soriano

------------------------------------------------

//BEFORE BREAKPOINT

//REGISTERED RESULT OF R2 MULTIPLIED BY R1
//HEXIDECIMAL 0
R3 = #0

//REGISTERED NUMBER MULTIPLIED BY CONSTANT, WITH VALUE HEXIDECIMAL 12
R2 = #12

//CONSTANT REGISTER, WITH VALUE HEXIDECIMAL 6
R1 = #6 

------------------------------------------------

//PROGRAM ENTERS BREAKPOINT

//WHEN R1 = #6
R3 = #0
R2 = #6

//WHEN R1 = #5
R3 = #12
R2 = #6

//WHEN R1 = #4
R3 = #24
R2 = #6

//WHEN R1 = #3
R3 = #36
R2 = #6

//WHEN R1 = #2
R3 = #48
R2 = #6

//WHEN R1 = #1
R3 = #60
R2 = #6

//WHEN R1 = #0
R3 = #72
R2 = #6

------------------------------------------------

//R1 = #0, PROGRAM BREAKS OUT OF AGAIN(LOOP)
R3 = #72
R2 = #6
R1 = #0